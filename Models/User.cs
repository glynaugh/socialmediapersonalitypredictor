﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RedditPersonalityAnalyzer.Models
{
    public class User
    {
        public string username { get; set; }
        public Dictionary<string, List<string>> topPersonalityCategoriesAndWords;
        public string errorMessage;

    }
}

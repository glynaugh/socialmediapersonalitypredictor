﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedditPersonalityAnalyzer.Models
{
    public class Error
    {
        public int statusCode { get; set; }
        public string errorMessage { get; set; }
    }
}

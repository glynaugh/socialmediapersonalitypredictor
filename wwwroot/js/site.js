﻿
window.onload = function () {
    $(document).ready(function () {

        $('#personalityform').validate({
            rules: {
                username: {
                    required: true,
                    pattern: '^[a-zA-Z0-9_-]+',
                    maxlength: 20
                }
            },
            messages: {
                username: {
                    pattern: "<div class=\"col-sm-offset-4\"></div>Invalid username format."
                }
            }
        });

        $('#textsampleform').validate({
            rules: {
                userinput: {
                    required: true,
                    maxlength: 100000
                }

            }
        });
        $('#feedbackform').validate({
            rules: {
                username: {
                    required: true,
                    pattern: '^[a-zA-Z0-9_-]+',
                    maxlength: 20
                },
                usercomment: {
                    required: true,
                    maxlength: 8000
                }

            }
        });

    });
}


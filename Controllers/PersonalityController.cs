﻿
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using RedditPersonalityAnalyzer.Models;
using RedditPersonalityAnalyzer.Service;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RedditPersonalityAnalyzer.Controllers
{

    public class PersonalityController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            var username = HttpContext.Request.Query["username"].ToString();
            try
            {
                User userCommentResults = UserPersonality.createUserModelWithPersonality(username);
                ViewBag.header1 = "This user is most likely to be...";
                ViewBag.header2 = "This user may also be...";
                return View(userCommentResults);
            }
            catch (SqlException)
            {
                Response.StatusCode = 500;
                var error = new Error();
                error.statusCode = 500;
                error.errorMessage = "Server error.";
                return View("~/Views/Shared/Error.cshtml", error);
            }


            catch (UserDoesNotExistException)
            {
                Response.StatusCode = 400;
                var error = new Error();
                error.statusCode = 400;
                error.errorMessage = "User does not exist.";
                return View("~/Views/Shared/Error.cshtml", error);
            }
          
        }

        [HttpPost]
        public IActionResult TextSamplePersonality()
        {
            var textSample = Request.Form["userinput"];
            try
            {
                User textSampleResults = TextSampleAnalysis.main(textSample);
                ViewBag.header1 = "This text suggests the writer may be...";
                ViewBag.header2 = "The writer may also be...";
                return View(textSampleResults);
            }
            catch (SqlException)
            {
                Response.StatusCode = 500;
                var error = new Error();
                error.statusCode = 500;
                error.errorMessage = "Server error.";
                return View("~/Views/Shared/Error.cshtml", error);
            }

            catch (System.AggregateException)
            {
                Response.StatusCode = 500;
                var error = new Error();
                error.statusCode = 500;
                error.errorMessage = "Server error.";
                return View("~/Views/Shared/Error.cshtml", error);
            }

        }

    }
}



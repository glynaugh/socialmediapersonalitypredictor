﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RedditPersonalityAnalyzer.Models;

namespace RedditPersonalityAnalyzer.Controllers
{
    public class ErrorController : Controller
    {
        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            var error = new Error();
            switch (statusCode)
            {
                case 404:
                    error.errorMessage = "Page not found.";
                    break;
                      
            }
            
            error.statusCode = statusCode;
            return View("~/Views/Shared/Error.cshtml", error);
        }
    }
}
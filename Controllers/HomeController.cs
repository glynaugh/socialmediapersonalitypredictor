﻿
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using RedditPersonalityAnalyzer.Models;
using RedditPersonalityAnalyzer.Service;

namespace RedditPersonalityAnalyzer.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Microservice()
        {
            return View();
        }

        public IActionResult Feedback()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SubmitFeedback()
        {
            var username = Request.Form["username"];
            var accuracy = Request.Form["accuracy"];
            var userComment = Request.Form["usercomment"];
            try
            {
                var userComments = new UserRedditComments(username).userCommentsInSingleString;
                DatabaseUpdater.updateDatabase(username, accuracy, userComment);
                return View("~/Views/Shared/ThankYou.cshtml");
            }
            catch (UserDoesNotExistException)
            {
                Response.StatusCode = 400;
                var error = new Error();
                error.statusCode = 400;
                error.errorMessage = "User does not exist.";
                return View("~/Views/Shared/Error.cshtml", error);
            }
            catch (ServerException e)
            {
                Response.StatusCode = 500;
                var error = new Error();
                error.statusCode = 500;
                error.errorMessage = e.errorMessage;
                return View("~/Views/Shared/Error.cshtml", error);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedditPersonalityAnalyzer.Models;

namespace RedditPersonalityAnalyzer.Service
{
    public class TextSampleAnalysis
    {

        public static User main(string textSample)
        {
            var userCategoriesAndWords = UserPersonality.getPersonalityResultFromLambda(textSample);
            var user = new User();
            user.topPersonalityCategoriesAndWords = userCategoriesAndWords;
            return user;
        }
    }
}

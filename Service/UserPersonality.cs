﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using RedditPersonalityAnalyzer.Models;

namespace RedditPersonalityAnalyzer.Service
{

    public class UserPersonality
    {
        public static string putUserInputIntoJson(string userInput)
        {
            var inputDict = new Dictionary<string, string>
            {
                {"inputstring", userInput}
            };
            var json = JsonConvert.SerializeObject(inputDict);
            return json;
        }

        public static string receiveJsonStringResultFromLambda(string requestJson)
        {
            var gatewayUrl = new AppInfo().appConnectionInfo.personalityMicroserviceInfo.analyzePersonalityGateway;
            var client = new RestClient(gatewayUrl);
            var request = new RestRequest("AnalyzePersonality");
            request.AddJsonBody(requestJson);
            var response = client.Post(request);
            return response.Content;
        }

        public static Dictionary<string, List<string>> convertResponseJsonToUserCategoriesAndWords(string responseJson)
        {
            var jsonResult = JsonConvert.DeserializeObject(responseJson).ToString();
            var wordsAndCategories = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(jsonResult);
            return wordsAndCategories;
        }

        public static Dictionary<string, List<string>> getPersonalityResultFromLambda(string userRedditComments)
        {
            var requestJson = putUserInputIntoJson(userRedditComments);
            var responseJson = receiveJsonStringResultFromLambda(requestJson);
            var userCategoriesAndWords = convertResponseJsonToUserCategoriesAndWords(responseJson);
            
            foreach (var word in userCategoriesAndWords.Keys.ToList())
            {
               var result = userCategoriesAndWords[word];
            }
            return userCategoriesAndWords;
        }

        public static User createUserModelWithPersonality(string username)
        {
            string userRedditCommentsInSingleString = new UserRedditComments(username).userCommentsInSingleString;
            var userCategoriesAndWords = getPersonalityResultFromLambda(userRedditCommentsInSingleString);
            var user = new User();
            user.username = username;
            user.topPersonalityCategoriesAndWords = userCategoriesAndWords;
            return user;
        }
    }
}

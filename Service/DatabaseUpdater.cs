﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using Newtonsoft.Json;

namespace RedditPersonalityAnalyzer.Service
{
    public class ServerException : Exception
    {
        public string errorMessage { get; }
        public ServerException(string message)
        {
            errorMessage = message;
        }
    }

    public class DatabaseUpdater
    {

        public static string addSQLEscapeCharacters(string comment)
        {
            return comment.Replace("'", "''");
        }


        public static void updateDatabase(string username, string accuracy, string userComment)
        {
            try
            {
                string modifiedUserComment = addSQLEscapeCharacters(userComment);
                var connectionStringFromFile = new AppInfo().appConnectionInfo.dbConnection.connectionString;

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = connectionStringFromFile;

                SqlConnection connection = new SqlConnection(builder.ConnectionString);
                using (connection)
                {
                    SqlCommand command = new SqlCommand("INSERT INTO feedback(username, accuracy, user_comment) VALUES(@username, @accuracy, @user_comment)", connection);
                    var paramDict = new Dictionary<string, string>
                    {
                        { "@username", username },
                        { "@accuracy", accuracy },
                        { "@user_comment", userComment }
                    };
                    foreach (var parameterNameAndValue in paramDict)
                    {
                        command.Parameters.AddWithValue(parameterNameAndValue.Key, parameterNameAndValue.Value);
                    };
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                    }
                }
            }
            catch
            {
                throw new ServerException("Error encountered in database access.");
            }
            

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace RedditPersonalityAnalyzer.Service
{
    public class AppConnectionInfo
    {
        public DbConnection dbConnection;
        public PersonalityMicroserviceInfo personalityMicroserviceInfo;
        public RedditAPIAccessInfo redditAPIAccessInfo;
    }

    public class DbConnection
    {
        public string connectionString;
    }

    public class PersonalityMicroserviceInfo
    {
        public string analyzePersonalityGateway;
    }

    public class RedditAPIAccessInfo
    {
        public string client_ID;
        public string secret;
    }


    public class AppInfo
    {
        public AppConnectionInfo appConnectionInfo;
        public AppInfo()
        {
            appConnectionInfo = JsonConvert.DeserializeObject<AppConnectionInfo>(File.ReadAllText("appsettings.json"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;
using Newtonsoft.Json;

namespace RedditPersonalityAnalyzer.Service
{
    public class UserDoesNotExistException : Exception
    {
        public UserDoesNotExistException()
        {
        }
    }

    public class AccessToken
    {
        public string access_token { get; set; }
    }

    public class Rootobject
    {
        public Data data { get; set; }
    }

    public class Data
    {
        public Child[] children { get; set; }
    }

    public class Child
    {
        public Data1 data { get; set; }
    }

    public class Data1
    {
        public string body { get; set; }
    }

    public class UserRedditComments
    {

        public string username;
        private Child[] userComments;
        public List<string> userCommentsInCollection;
        public string userCommentsInSingleString;

        public UserRedditComments(string user)
        {
            username = user;
            userComments = getUserComments(user);
            userCommentsInCollection = putUserCommentsInCollection(userComments);
            userCommentsInSingleString = convertUserCommentsToSingleString(userComments);
        }

        private static string getRedditAccessToken()
        {
            var client = new RestClient("https://www.reddit.com/api/v1/access_token");
            var appInfo = new AppInfo();
            var client_ID = appInfo.appConnectionInfo.redditAPIAccessInfo.client_ID;
            var secret = appInfo.appConnectionInfo.redditAPIAccessInfo.secret;
            client.Authenticator = new HttpBasicAuthenticator(client_ID, secret);
            var request = new RestRequest("");
            request.AddParameter("grant_type", "client_credentials");
            var response = client.Post(request);
            AccessToken accessToken = new JsonDeserializer().Deserialize<AccessToken>(response);
            return accessToken.access_token;
        }

        private static Child[] getUserComments(string username)
        {
            string accessToken = getRedditAccessToken();
            string appendedAccessToken = "bearer " + accessToken;
            var client = new RestClient("https://oauth.reddit.com/");
            string requestURL = "user/" + username + "/comments";
            var request = new RestRequest(requestURL).AddParameter("limit", "100", ParameterType.QueryString);
            request.AddHeader("Authorization", appendedAccessToken);
            request.AddHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0");
            var response = client.Get(request);
            Rootobject rootData = JsonConvert.DeserializeObject<Rootobject>(response.Content);
            if (rootData.data == null)
            {
                throw new UserDoesNotExistException();
            }
            return rootData.data.children;
        }

        private static List<string> putUserCommentsInCollection(Child[] userComments)
        {
            List<string> userCommentsList = new List<string>();
            foreach (Child childWithData in userComments)
            {
                string comment = childWithData.data.body;
                userCommentsList.Add(comment);
            }
            return userCommentsList;
        }

        private static string convertUserCommentsToSingleString(Child[] userComments)
        {
            string combinedComments = "";
            foreach (Child childWithData in userComments)
            {
                string comment = childWithData.data.body;
                combinedComments += " " + comment;
            }
            return combinedComments;
        }
    }
}
